import sys
if len(sys.argv) == 1:
    print('You are expected to provide a problem number as argument.')
else:
    print('Running problem', sys.argv[1] + '...')
    __import__('problems.' + sys.argv[1])

from util.manips import reverse


def isPalindrome(n):
    return n == reverse(n)


def isPermutation(a, b):
    return ''.join(sorted(str(a))) == ''.join(sorted(str(b)))


def isPandigital(n):
    return ''.join(sorted(str(n))) == "123456789"


def isTriangle(n):
    delta = .25 + 2 * n  # solve 0.5(x² + x) - n = 0
    r1 = -.5 + delta**.5  # r2 will be < 0
    return r1.is_integer()  # n has to be a natural number


def isPentagonal(n):
    delta = .25 + 6 * n  # solve 1.5x² - 0.5x - n = 0
    r1 = (.5 + delta**.5) / 3  # r2 will be < 0
    return r1.is_integer()  # n has to be a natural number


def isHexagonal(n):
    delta = 1 + 8 * n  # solve 2x² - x - n = 0
    r1 = (1 + delta**.5) / 4  # r2 will be < 0
    return r1.is_integer()  # n has to be a natural number

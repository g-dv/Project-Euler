from math import *


def nDigits(x):
    if x == 0:
        return 1
    return 1 + int(log(x, 10))


def reverse(n):
    res = 0
    while n:
        res *= 10
        res += n % 10
        n //= 10
    return res


def concat(numbers):
    res = 0
    for n in numbers:
        res *= 10**nDigits(n)
        res += n
    return res


def cumulativeSum(container):
    cumulativeSum = [0]
    for e in container:
        cumulativeSum.append(cumulativeSum[-1] + e)
    return cumulativeSum


def nCr(n, r):
    return factorial(n) // factorial(r) // factorial(n-r)


def sumDigits(n):
    res = 0
    while n:
        res += n % 10
        n //= 10
    return res


def firstDigits(n, nDigits):
    return n // 10**(int(log(n, 10)) - nDigits + 1)


def product(container):
    res = 1
    for e in container:
        res *= e
    return res


def primitivePythagoreanTriples(limit=None):
    # optimized algorithm from StackOverflow
    import numpy as np
    u = np.mat(' 1  2  2; -2 -1 -2; 2 2 3')
    a = np.mat(' 1  2  2;  2  1  2; 2 2 3')
    d = np.mat('-1 -2 -2;  2  1  2; 2 2 3')
    uad = np.array([u, a, d])
    m = np.array([3, 4, 5])
    while m.size:
        m = m.reshape(-1, 3)
        if limit:
            m = m[m[:, 2] <= limit]
        yield from m
        m = np.dot(m, uad)


def pythagoreanTriples(limit):
    # optimized algorithm from StackOverflow
    for prim in primitivePythagoreanTriples(limit):
        i = prim
        for _ in range(limit//prim[2]):
            yield i
            i = i + prim

import io
from contextlib import redirect_stdout
import time

with open('data/test-data.txt') as file:
    testData = file.readlines()
testData = [int(i) for i in testData]

totalTime = time.time()
for i, expected in enumerate(testData):
    f = io.StringIO()
    with redirect_stdout(f):
        startTime = time.time()
        __import__('problems.' + '{:03d}'.format(i + 1))
        duration = time.time() - startTime
    out = int(float(f.getvalue()))

    no = '{:03d}'.format(i + 1)
    if out == expected:
        result = 'OK'
    else:
        result = 'FAILED'
    duration = '(' + str('{:06.3f}'.format(duration)) + 's)'
    print(no, result, duration, 'expected', expected, 'got', int(float(out)))

totalTime = time.time() - totalTime
averageTime = '{:04.2f}'.format(totalTime / len(testData))
totalTime = int(totalTime)
print('Total time:', totalTime, 'sec. (avg:', averageTime, 'sec)')

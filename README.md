# Project-Euler

Personal collection of python 3 solutions to Project Euler problems (from https://projecteuler.net)

## Regarding comments

I put little to no comments since:

* I'm working alone on these problems.
* I don't share these solutions.
* I don't usually need to go back and understand old problems.

## Test

Run `test.py` to test all the solutions. It may take some time.

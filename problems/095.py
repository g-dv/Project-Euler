from util.factors import factors

# This method is not really good but despite quite
# a lot of thinking, I didn't find anything smarter.

LIMIT = 1000000

transitions = [None] * (LIMIT + 1)
for n in range(LIMIT):
    transitions[n] = sum(factors(n))

visited = [False] * (LIMIT + 1)
bestSolution = -1
for n in range(LIMIT):
    chainLength = 0
    if not visited[n]:
        visited[n] == True
        chain = []
        nextInt = n
        while True:
            chainLength += 1
            nextInt = transitions[nextInt]
            # amicable chain found
            if nextInt == n:
                if chainLength > bestSolution:
                    res = n
                    bestSolution = chainLength
                break
            # the chain goes off-limits
            if nextInt > LIMIT:
                break
            # we already visited that path from here
            if visited[nextInt]:
                # we found a loop but its beginning isn't "n"
                if nextInt in chain:
                    for i, e in enumerate(chain):
                        if e == nextInt:
                            chainLength = len(chain[i:])
                            if chainLength > bestSolution:
                                res = min(chain[i:])
                                bestSolution = chainLength
                            break
                break
            visited[nextInt] = True
            chain.append(nextInt)
print(res)

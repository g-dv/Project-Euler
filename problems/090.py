from itertools import combinations


def isValid(diceA, diceB):
    for target in TARGETS:
        a = target % 10
        b = target // 10
        if not (a in diceA and b in diceB):
            if not (a in diceB and b in diceA):
                return False
    return True


TARGETS = [1, 4, 6, 16, 25, 36, 46, 81]

dices = list(combinations([0, 1, 2, 3, 4, 5, 6, 6, 7, 8], 6))
res = 0
for i, a in enumerate(dices):
    for b in dices[i:]:
        res += isValid(a, b)
print(res)

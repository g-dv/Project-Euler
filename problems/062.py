class PerfectCube:
    def __init__(self, smallestRoot):
        self.count = 1
        self.smallestRoot = smallestRoot


perfectCubes = {}
i = 340
while True:
    key = ''.join(sorted(str(i**3)))
    if key in perfectCubes:
        perfectCubes[key].count += 1
        if perfectCubes[key].count == 5:
            print(perfectCubes[key].smallestRoot)
            break
    else:
        perfectCubes[key] = PerfectCube(i**3)
    i += 1

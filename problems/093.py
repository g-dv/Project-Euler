from itertools import combinations, permutations
from util.manips import concat


def evaluate(schema):
    global targets
    stack = []
    for e in schema:
        if isinstance(e, int):
            stack.append(e)
        else:
            if e == "+":
                stack.append(stack.pop() + stack.pop())
            elif e == "-":
                stack.append(stack.pop() - stack.pop())
            elif e == "*":
                stack.append(stack.pop() * stack.pop())
            elif e == "/":
                a = stack.pop()
                b = stack.pop()
                if b:
                    stack.append(a / b)
                else: # division by 0
                    return
    if stack[0] == int(stack[0]) and stack[0] >= 0:
        targets[int(stack[0])] = True


def fillAndCalculate(schema, depth=0):
    if depth == 3:
        evaluate(schema)
        return
    operatorPosition = depth
    for i, e in enumerate(schema): # 4^3 operators
        if e in ["+", "-", "*", "/", ""]:
            if not operatorPosition:
                operatorPosition = i
                break
            else:
                operatorPosition -= 1
    for e in ["+", "-", "*", "/"]:
        schema[operatorPosition] = e
        fillAndCalculate(schema, depth + 1)


bestValue = -1
for combination in combinations([1, 2, 3, 4, 5, 6, 7, 8, 9], 4): # 9C4 sets
    targets = [False] * 5000
    for p in permutations(combination): # 4! positions
        fillAndCalculate([p[0], p[1], "",   p[2], "",   p[3], ""]) # 5 parenthesis
        fillAndCalculate([p[0], p[1], "",   p[2], p[3], "",   ""])
        fillAndCalculate([p[0], p[1], p[2], "",   "",   p[3], ""])
        fillAndCalculate([p[0], p[1], p[2], "",   p[3], "",   ""])
        fillAndCalculate([p[0], p[1], p[2], p[3], "",   "",   ""])
    for i in range(5000):
        if not targets[i]:
            if i > bestValue:
                bestValue = i
                res = p
            break
print(concat(sorted(res)))

# 9C4 sets
# 4! positions per set
# 5 options for parenthesis:
#     (((12)3)4) = 1 2 op 3 op 4 op
#     ((12)(34)) = 1 2 op 3 4 op op
#     ((1(23))4) = 1 2 3 op op 4 op
#     (1((23)4)) = 1 2 3 op 4 op op
#     (1(2(34))) = 1 2 3 4 op op op
# 4^3 options for operators
# total = 9C4 * 4! * 5 * 4^3 = 126 * 24 * 5 * 64 = 967680

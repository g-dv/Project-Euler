primes = [True] * 2000000
for i in range(2, int(2000000**0.5)):
    if primes[i]:
        for j in range(2 * i, len(primes), i):
            primes[j] = False
res = -1  # 0 and 1 aren't primes
for i, isPrime in enumerate(primes):
    if isPrime:
        res += i
print(res)

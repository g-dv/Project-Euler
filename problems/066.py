def getContinuousFraction(n):  # cf. p64
    a0 = int(n**.5)
    if a0 == n**.5:
        return []
    a = a0
    b = 1.0
    c = 0.0
    terms = [a0]
    while a != 2 * a0:
        c = b * a - c
        b = (n - c * c) / b
        a = int((a0 + c) / b)
        terms.append(a)
    return terms[:-1]


def getDenominator(cf):  # simplify fraction
    numerator = 1
    denominator = cf[-1]
    for i in cf[-2::-1]:
        denominator = denominator * i + numerator
        numerator = denominator
    return denominator


maxDenominator = -1
for n in range(1, 1001):
    if n**.5 != int(n**.5):
        terms = getContinuousFraction(n)
        if len(terms) % 2:
            denominator = 2 * getDenominator(terms)**2 + 1
        else:
            denominator = getDenominator(terms)
        if denominator > maxDenominator:
            maxDenominator = denominator
            res = n
print(res)

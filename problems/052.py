i = 100
res = False
while not res:
    j = i
    i *= 10
    while j * 6 < i and not res:
        k = 2
        while sorted(str(j)) == sorted(str(j * k)):
            k += 1
            if k == 6:
                res = j
        j += 1
print(res)

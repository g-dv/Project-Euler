from util.factors import isPrime, primesBelow
from util.manips import cumulativeSum

# spare the calculation of sum(sequence) each time
cumulativeSum = cumulativeSum(primesBelow(10**6))

maxLength = 1
for i in range(0, len(cumulativeSum)):
    for j in range(i + maxLength, len(cumulativeSum)):
        sumSequence = cumulativeSum[j] - cumulativeSum[i]
        if sumSequence > 10**6:
            break
        if isPrime(sumSequence):
            maxLength = j - i
            res = sumSequence
print(res)

from util.factors import isPrime


def satisfiesConjecture(n):
    i = 1
    while i < (n / 2)**.5:
        if isPrime(n - 2 * i * i):
            return True
        i += 1
    return False


n = 9
while True:
    if not isPrime(n):
        if not satisfiesConjecture(n):
            print(n)
            break
    n += 2

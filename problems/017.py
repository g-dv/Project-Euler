# 1 - 9
sum1to9 = len('onetwothreefourfivesixseveneightnine')

# 10 - 19
sum10to19 = len(
    'teneleventwelvethirteenfourteenfifteensixteenseventeeneighteennineteen')

# 20 - 99
sum20to99 = 10 * len(
    'twentythirtyfortyfiftysixtyseventyeightyninety') + 8 * sum1to9

# 1 - 99
sum1to99 = sum1to9 + sum10to19 + sum20to99

# 1 - 999
sum100to999 = (sum1to9 * 100) + (900 * len('hundred')) + (891 * len('and')) + (
    10 * sum1to99)

# 1 - 1000
sum1to1000 = sum100to999 + len('one') + len('thousand')

print(sum1to1000)

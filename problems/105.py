from itertools import combinations


def subsets(s):
    res = []
    for i in range(len(s)):
        for subset in combinations(s, i + 1):
            res.append(set(subset))
    return res


def isSpecialSumSet(a):
    for b in subsets(a):
        for c in subsets(a - b):
            if sum(b) == sum(c):
                return False
            if len(b) > len(c) and not sum(b) > sum(c):
                return False
    return True


file = open('data/105.txt', 'r')
sets = [set(int(s) for s in line.split(',')) for line in file.readlines()]
file.close()
res = 0
for e in sets:
    if isSpecialSumSet(e):
        res += sum(e)
print(res)

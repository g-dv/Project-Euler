file = open('data/54.txt', 'r')
games = file.readlines()
file.close()

VALUES = {
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    'T': 10,
    'J': 11,
    'Q': 12,
    'K': 13,
    'A': 14
}

COMBO_TYPES = {
    'NONE': 0,
    'ONE_PAIR': 1,
    'TWO_PAIRS': 2,
    'THREE_OF_A_KIND': 3,
    'STRAIGHT': 4,
    'FLUSH': 5,
    'FULL_HOUSE': 6,
    'FOUR_OF_A_KIND': 7,
    'STRAIGHT_FLUSH': 8,
    'ROYAL_FLUSH': 9,
}


class Card:
    def __init__(self, value, suite):
        self.value = value
        self.suite = suite


class Score:
    def __init__(self, hand):
        self.comboType, self.comboValue = scoreRoyalFlush(
            hand, getGroups(hand))
        self.comboType = COMBO_TYPES[self.comboType]
        sortedUnused = [
            0 if e.value == self.comboValue else e.value for e in hand
        ]
        sortedUnused.sort(reverse=True)
        self.sortedUnused = sortedUnused

    def toInt(self):
        score = str(self.comboType)
        score += str(self.comboValue + 10)  # +10 to make sure we have 2 digits
        for e in self.sortedUnused:
            score += str(10 + e)  # idem
        return int(score)


class Group:
    def __init__(self, size=1, value=0):
        self.size = size
        self.value = value


class Groups:
    def __init__(self):
        self.primary = Group()
        self.secondary = Group()


def getGroups(hand):
    counters = [0] * 15
    for card in hand:
        counters[card.value] += 1
    groups = Groups()
    for i, e in enumerate(counters):
        if e == max(counters) and groups.primary.size == 1:
            groups.primary = Group(e, i)
        elif e > 1:
            groups.secondary = Group(e, i)
    return groups


def hasFlush(hand):
    for card in hand[1:]:
        if card.suite != hand[0].suite:
            return False
    return True


def getFirstValue(hand):
    counters = [0] * 15
    for card in hand:
        counters[card.value] += 1
    for i, e in enumerate(counters):
        if e == 1:
            return i, counters
    return -1, counters


def hasStraight(hand):
    i, counters = getFirstValue(hand)
    return i < 11 and [counters[i]] * 4 == counters[i + 1:i + 5]


def getHighestStraightValue(hand):
    i, counters = getFirstValue(hand)
    return i + 4


def scoreOnePair(hand, groups):
    if groups.primary.size == 2 and groups.secondary.size == 1:
        return 'ONE_PAIR', groups.primary.value
    return 'NONE', 0


def scoreTwoPairs(hand, groups):
    if groups.primary.size == 2 and groups.secondary.size == 2:
        return 'TWO_PAIRS', groups.primary.value
    return scoreOnePair(hand, groups)


def scoreThreeOfAKind(hand, groups):
    if groups.primary.size == 3 and groups.secondary.size == 1:
        return 'THREE_OF_A_KIND', groups.primary.value
    return scoreTwoPairs(hand, groups)


def scoreStraight(hand, groups):
    if hasStraight(hand):
        return 'STRAIGHT', getHighestStraightValue(hand)
    return scoreThreeOfAKind(hand, groups)


def scoreFlush(hand, groups):
    if hasFlush(hand):
        return 'FLUSH', 0
    return scoreStraight(hand, groups)


def scoreFullHouse(hand, groups):
    if groups.primary.size == 3 and groups.secondary.size == 2:
        return 'FULL_HOUSE', groups.primary.value
    return scoreFlush(hand, groups)


def scoreFourOfAKind(hand, groups):
    if groups.primary.size == 4 and groups.secondary.size == 1:
        return 'FOUR_OF_A_KIND', groups.primary.value
    return scoreFullHouse(hand, groups)


def scoreStraightFlush(hand, groups):
    if hasStraight(hand) and hasFlush(hand):
        return 'STRAIGHT_FLUSH', getHighestStraightValue(hand)
    return scoreFourOfAKind(hand, groups)


def scoreRoyalFlush(hand, groups):
    if hasStraight(hand) and hasFlush(hand) and max([e.value
                                                     for e in hand]) == 14:
        return 'ROYAL_FLUSH', getHighestStraightValue(hand)
    return scoreStraightFlush(hand, groups)


res = 0
for game in games:
    p1Hand = [Card(VALUES[e[0]], e[1]) for e in game[0:14].split(' ')]
    p2Hand = [Card(VALUES[e[0]], e[1]) for e in game[15:29].split(' ')]
    if Score(p1Hand).toInt() > Score(p2Hand).toInt():
        res += 1
print(res)

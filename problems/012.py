i = 1
triangleNumber = 1
nFactors = 0
while nFactors <= 500:
    i += 1
    triangleNumber += i
    power = triangleNumber**.5
    nFactors = int(pow(triangleNumber, .5)) == power
    for factor in range(1, int(power)):
        nFactors += 2 * (not triangleNumber % factor)
print(triangleNumber)

from util.factors import primeGenerator


def solve(p, generator, memory): # cf. p76 which is almost identical
    for i in range(p, 101):
        memory[i] += memory[i - p]
    nextPrime = next(generator)
    for i in range(p, nextPrime):
        if memory[i] > 5000:
            return i
    return solve(nextPrime, generator, memory)


generator = primeGenerator()
print(solve(next(generator), generator, [1] + [0] * 100))

from numpy.linalg import inv
from numpy import dot


def u(n):
    return 1 - n + n**2 - n**3 + n**4 - n**5 + n**6 - n**7 + n**8 - n**9 + n**10


res = 0
for i in range(1, 11):
    matrix = []
    y = []
    for j in range(1, i + 1):
        line = []
        for k in range(0, i):
            line.append(j**(i - k - 1))
        matrix.append(line)
        y.append([u(j)])
    for k, a in enumerate([e[0] for e in dot(inv(matrix), y)]):
        res += a * (i + 1)**(i - k - 1)
print(round(int(res)))

# I simply solved it with: http://mth.bz/fit/polyfit/findcurv.htm

import math
from util.manips import nDigits


def sumFactorials(x):
    sum = 0
    while x:
        sum += math.factorial(x % 10)
        x //= 10
    return sum


res = 0
i = 3
while True:
    if math.factorial(9) * nDigits(i) < i:
        break
    if sumFactorials(i) == i:
        res += i
    i += 1
print(res)

# to find the maximum number for which it could be possible
# we have to consider that the sum of factorials cannot
# exceed: 9! * nDigits
# where nDigits(x) = 1 + floor(log10(x))
# so we can stop the search when
# x > nDigits(x) * 9!
# since after that limit, the sum will always be less than
# the number

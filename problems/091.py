# Trivial method but it's good enough and I dislike this problem.

LIMIT = 51
res = 0
for x1 in range(LIMIT):
    for y1 in range(LIMIT):
        a = x1 * x1 + y1 * y1
        for x2 in range(LIMIT):
            for y2 in range(LIMIT):
                b = x2 * x2 + y2 * y2
                deltaX = x2 - x1
                deltaY = y2 - y1
                c = deltaX * deltaX + deltaY * deltaY
                if a != 0 and b != 0 and c != 0:
                    if a == b + c or b == a + c or c == a + b:
                        res += 1
print(res // 2)

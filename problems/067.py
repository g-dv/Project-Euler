file = open('data/67.txt', 'r')
lines = list(file.readlines())
file.close()

inputTriangle = [list([int(e) for e in line.split(" ")]) for line in lines]

dp = [inputTriangle[0]]
for i in range(1, len(inputTriangle)):
    dp.append(inputTriangle[i])
    for j, partialSum in enumerate(dp[i]):
        if j == 0:
            dp[i][j] += dp[i - 1][j]
        elif j == i:
            dp[i][j] += dp[i - 1][j - 1]
        else:
            dp[i][j] += max(dp[i - 1][j - 1], dp[i - 1][j])

print(max(dp[-1]))

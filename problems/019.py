# january = 0 -> december = 11
# monday = 0 -> sunday = 6


# return the length of the month
def getMonthLength(month, year):
    if month in [0, 2, 4, 6, 7, 9, 11]:
        return 31
    if month in [3, 5, 8, 10]:
        return 30
    if not year % 4 and year % 100 or not year % 400:
        return 29
    return 28


res = 0

month = 0
day = 1
year = 1901
weekDay = 1  # 1901-01-01 was a tuesday
while month != 11 or year != 2000 or day != 31:
    # sunday falling on the first of the month
    if day == 1 and weekDay == 6:
        res += 1
    # increment day
    weekDay = (weekDay + 1) % 7
    day += 1
    if day > getMonthLength(month, year):
        month += 1
        day = 1
    if month == 12:
        month = 0
        year += 1

print(res)

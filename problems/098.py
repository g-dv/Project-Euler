from itertools import permutations

# Brute force.
# It would take around 20 seconds without my "lucky" shortcut.


def areAnagrams(a, b):
    return ",".join(sorted(a)) == ",".join(sorted(b))


def mapCharacters(pair):
    positions = dict()
    for i, e in enumerate(set(pair[0])):
        positions[e] = i
    maps = [[0] * 5, [0] * 5]
    for i, e in enumerate(pair[0][::-1]):
        maps[0][positions[e]] += 10**i
    for i, e in enumerate(pair[1][::-1]):
        maps[1][positions[e]] += 10**i
    return maps, [positions[pair[0][0]], positions[pair[1][0]]]


def readData():
    file = open('data/98.txt', 'r')
    words = list(eval(file.read()))
    file.close()
    return words


def isSquare(n):
    return n**.5 == int(n**.5)


words = readData()
sortedWords = [",".join(sorted(word)) for word in words]
anagrams = []
for word in words:
    if sortedWords.count(",".join(sorted(word))) > 1:
        anagrams.append(word)

pairs = []
for i, a in enumerate(anagrams):
    for b in anagrams[i + 1:]:
        if areAnagrams(a, b):
            pairs.append([a, b])

res = -1
numbers = list(permutations([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 5))
for pair in pairs:
    if len(set(pair[0])) != 5: # shortcut - print "pairs" to understand
        continue
    maps, firstLetterPositions = mapCharacters(pair)
    for permutation in numbers:
        if permutation[firstLetterPositions[0]] == 0:
            continue
        if permutation[firstLetterPositions[1]] == 0:
            continue
        a = b = 0
        for i, n in enumerate(permutation):
            a += maps[0][i] * n
            b += maps[1][i] * n
        if isSquare(a) and isSquare(b):
            res = max([res, a, b])
print(res)

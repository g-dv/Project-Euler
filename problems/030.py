import math
from util.manips import nDigits


def getSumOfDigits(x):
    sum = 0
    while x:
        sum += (x % 10)**5
        x //= 10
    return sum


res = 0
i = 10
while True:
    if (9**5) * nDigits(i) < i:
        break
    if i == getSumOfDigits(i):
        res += i
    i += 1
print(res)

# to find the maximum number for which it could be possible
# we have to consider that the sum of digits power 5 cannot
# exceed: (9 ^ 5) * nDigits
# where nDigits(x) = 1 + floor(log10(x))
# so we can stop the search when
# x > nDigits(x) * 9 ^ 5
# since after that limit, the sum will always be less than
# the number

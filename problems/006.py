LIMIT = 100
sum = (LIMIT + 1) * LIMIT / 2
squareOfSum = pow(sum, 2)
sumOfSquares = 0
for i in range(1, LIMIT + 1):
    sumOfSquares += i * i
print(squareOfSum - sumOfSquares)

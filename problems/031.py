# dynamic programming - coin change
COINS = [1, 2, 5, 10, 20, 50, 100, 200]
TARGET = 200

solutions = [1] * (TARGET + 1)
for coin in COINS[1:]:
    for i in range(coin, len(solutions)):
        solutions[i] += solutions[i - coin]
print(solutions[-1])

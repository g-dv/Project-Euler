import math
from util.factors import factors

LIMIT = 28123


def isAbundant(x):
    return sum(factors(x)) > x


# list the abundant numbers
abundants = []
for i in range(1, LIMIT):
    if isAbundant(i):
        abundants.append(i)

# remove the numbers that can be expressed as a sum of two abundant numbers
possibleAnswers = [True] * LIMIT
for i in range(len(abundants)):
    for j in range(i, len(abundants) - i):
        sum = abundants[i] + abundants[j]
        if sum < LIMIT:
            possibleAnswers[sum] = False

# sum the remaining numbers
res = 0
for i in range(LIMIT):
    res += i * possibleAnswers[i]

print(res)

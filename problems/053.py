from util.manips import nCr

res = 0
for n in range(1, 101):
    for r in range(1, n): # 1 when n = r
        res += nCr(n, r) > 1000000
print(res)

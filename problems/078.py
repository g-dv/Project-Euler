def pentagonalGenerator():
    i = 1
    while True:
        yield (3 * i * i - i) // 2
        yield (3 * i * i + i) // 2
        i += 1


def signGenerator():
    while True:
        yield 1
        yield 1
        yield -1
        yield -1


nPartitions = [1]
i = 0
while nPartitions[-1] % 1000000:
    generator = pentagonalGenerator()
    sign = signGenerator()
    nPartitions.append(0)
    pentagonal = next(generator)
    i += 1
    while i + 1 > pentagonal:
        nPartitions[-1] += next(sign) * nPartitions[i - pentagonal]
        pentagonal = next(generator)
print(i)

# https://fr.wikipedia.org/wiki/Partition_d%27un_entier#Relation_de_r%C3%A9currence

from util.factors import isPrime
from util.manips import concat
from itertools import product


def getPartiallyFilledMasks(mask, fillers):
    """Fill the mask in every possible way using the lists
    of numbers contained in 'fillers'"""
    res = []
    for combination in fillers:
        j = 0
        partiallyFilledMask = []
        for i in range(len(mask)):
            if mask[i]:
                partiallyFilledMask.append(combination[j])
                j += 1
            else:
                partiallyFilledMask.append(False)
        res.append(partiallyFilledMask)
    return res


def getFillers(length):
    """Generate a list of lists of numbers that can be used to fill"""
    # optimization: the last digit of a prime has to be 1, 3, 7 or 9
    # so we fill the last digit separately
    res = []
    if length:
        for firstPart in [
                list(e) for e in product(range(0, 10), repeat=length - 1)
        ]:
            for lastDigit in [1, 3, 7, 9]:
                res.append(firstPart + [lastDigit])
    return res


def getMasks(length):
    """Generate all masks. We put a False if this digit is supposed to be in
    the group and a True if it can be filled by (almost) any digit"""
    res = []
    for mask in list(product([True, False], repeat=length)):
        # can't end with a False (cf. (i))
        # can't contain more less than three 3's (cf. (ii))
        if mask[-1] == True and mask.count(False) >= 3:
            res.append(list(mask))  # the mask is valid
    return res


def tryAllFills(mask):
    nToFill = mask.count(True)
    # replace the True by different digits
    for combination in getPartiallyFilledMasks(mask, fillers[nToFill]):
        nPrimes = 0
        # now replace the False by groups of 1, 2, etc.
        for i in range(0, 10):
            # replace the all the False by i
            number = [digit if digit else i for digit in combination]
            if isPrime(concat(number)):
                nPrimes += 1
                if nPrimes == 1:
                    res = concat(number)
                if nPrimes == 8:
                    return res
    return False


# I guess the result won't have more than 7 digits and there are at
# least 3 False. So 7 - 3 = 4 digits need to be filled at most.
fillers = [getFillers(i) for i in range(0, 5)]
res = False
length = 5
while not res:
    for mask in getMasks(length):  # for each mask
        res = tryAllFills(mask)  # see if we can get the answer out of it
        if res:
            break
    length += 1  # try again with larger numbers
print(res)

# There are other optimizations we could do, like avoid the starting zeros.
# We optimized in two ways:
# (i)  We noticed that if the last digit is in the group (ie. the last element
#      of the mask is False), then we can't find 8 primes since a prime can't
#      end with 0, 2, 4, 5, 6 or 8.
# (ii) We noticed that if there are only one or two digits that are in the
#      group, then we can't find 8 primes since the sum of the numbers will be
#      divisible by 3 too many times.

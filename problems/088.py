from collections import defaultdict


def backtrack(product, factorsSum, factorsNumber, lastFactor):
    if product < MAX_K + MAX_DIFFERENCE:
        memory[product].append(factorsSum - factorsNumber)
        for i in range(2, lastFactor + 1):
            backtrack(product * i, factorsSum + i, factorsNumber + 1, i)


MAX_K = 12000
MAX_DIFFERENCE = 550 # arbitrary - supposed max difference between k and sum
MAX_FACTOR = 450 # arbitrary - supposed upper bound for max factor

memory = defaultdict(list)

for i in range(2, MAX_FACTOR):
    backtrack(i, i, 1, i) # fill memory

productSumNumbers = set()
for i in range(2, MAX_K + 1): # for each k
    found = False
    offset = 0
    while not found and offset < MAX_DIFFERENCE:
        potentialSolutions = memory[i + offset]
        if offset in potentialSolutions: # ie. sum == product
            productSumNumbers.add(offset + i)
            found = True
            break
        offset += 1

print(sum(productSumNumbers))

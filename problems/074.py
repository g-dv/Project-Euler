from math import factorial


def sumFactorialOfDigits(n):
    total = 0
    while n != 0:
        total += factorial(n % 10)
        n //= 10
    return total


memory = dict()
res = 0
for n in range(1, 1000001):
    if n in memory:
        length = memory[n]
    else:
        length = 1
        last = sumFactorialOfDigits(n)
        chain = [n]
        while not last in chain: # while we haven't looped
            if last in memory: # if we reached a known value
                chain.append(last)
                length += memory[last]
                break
            chain.append(last) 
            last = sumFactorialOfDigits(last)
            length += 1

        # we found not only the length for e but for every element in 'chain'
        for e in chain[:chain.index(last)]:
            # length of e is cycle length + length between e and the cycle
            memory[e] = length - chain.index(e)
        for e in chain[chain.index(last):]:
            # e is in the cicle so its length is the cycle length
            memory[e] = length - chain.index(last)
    res += length == 60
print(res)

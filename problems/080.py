from decimal import *
from util.manips import sumDigits

getcontext().prec = 110

res = 0
for i in range(1, 101):
    if i**.5 != int(i**.5):
        x = Decimal(i).sqrt()
        while x <= 10**99:
            x *= 10
        res += sumDigits(int(x))
print(res)

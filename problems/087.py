from util.factors import primesBelow

LIMIT = 50000000

primes = primesBelow(7071) # 2root(LIMIT)

res = 0
found = [False] * LIMIT
for a in primes:
    s1 = a * a
    for b in primes[:368]: # 3root(LIMIT)
        s2 = s1 + b * b * b
        if s2 >= LIMIT:
            break
        for c in primes[:84]: # 2root(LIMIT)
            s3 = s2 + c * c * c * c
            if s3 >= LIMIT:
                break
            else:
                if not found[s3]:
                    res += 1
                    found[s3] = True
print(res)

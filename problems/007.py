primes = []
i = 2
while len(primes) != 10001:
    isPrime = True
    for prime in primes:
        if prime > pow(i, .5) or not isPrime:
            break
        isPrime = i % prime
    if isPrime:
        primes.append(i)
    i += 1
print(primes[-1])

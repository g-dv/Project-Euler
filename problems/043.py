from itertools import permutations

res = 0
pandigitals = list(permutations('0123456789'))
for pandigital in pandigitals:
    isValid = True
    string = ''.join(pandigital)
    for i, e in enumerate([2, 3, 5, 7, 11, 13, 17]):
        if int(string[i + 1:i + 4]) % e:
            isValid = False
            break
    if isValid:
        res += int(string)
print(res)

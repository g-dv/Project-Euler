from util.manips import pythagoreanTriples

def nStepsBetween(a, b):
    if a > b:
        return 0
    return 1 + int((b - a) / 2)

SQRT_5 = 2.2361

inf = 1 # dichotomic search
sup = 10000
while (sup - inf != 1):
    limit = int((inf + sup) / 2)
    nCuboids = 0
    for e in pythagoreanTriples(int(SQRT_5 * limit)):
        a = min(e[0], e[1])
        b = max(e[0], e[1])
        if a <= limit:
            nCuboids += nStepsBetween(b - a, a)
            if b <= limit:
                nCuboids += nStepsBetween(1, a - 1)
    if nCuboids < 1000000:
        inf = limit
    else:
        sup = limit
print(sup)

# let P be the Pythagorean triplets
# we find that pathLength = sqrt(x² + (y + z)²)
# so we must find the values:
#     (x, y, z) ∈ ℕ^3, (x, y + z, pathLength) ∈ P
# so, for each (a, b, c) ∈ P such as:
#     a <= b <= c
#     a <= LIMIT
#     b <= 2 * LIMIT
#     c <= SQRT_5 * LIMIT
# we must count the number of ways we can make a and b with x, y and z.
# eg. for (a, b, c) = (40, 75, 85) ∈ P
# we can find 20 + 3 = 23 solutions to our problem:
#     (1, 39, 75)
#     (2, 38, 75)
#     (3, 37, 75)
#     ...
#     (20, 20, 75)
#     and
#     (35, 40, 40)
#     (36, 39, 40)
#     (37, 38, 40)

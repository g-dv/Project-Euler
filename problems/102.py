class Point:
    def __init__(self, coordinates):
        self.x = coordinates[0]
        self.y = coordinates[1]


def triangleContainsOrigin(a, b, c):
    return 10**(-5) > abs(
        abs(a.x * b.y - b.x * a.y) +
        abs(c.x * a.y - a.x * c.y) +
        abs(b.x * c.y - c.x * b.y) -
        abs(a.x * (b.y - c.y) + b.x * (c.y - a.y) + c.x * (a.y - b.y))
    )


file = open("data/102.txt")
lines = [[int(s) for s in line.split(',')] for line in file.readlines()]
file.close()

res = 0
for line in lines:
    a = Point(line[0:2])
    b = Point(line[2:4])
    c = Point(line[4:6])
    res += triangleContainsOrigin(a, b, c)
print(res)

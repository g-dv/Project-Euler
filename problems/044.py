from os import sys
from util.properties import isPentagonal


def P(n):
    return n * (3 * n - 1) / 2


i = 2
while i:
    for j in range(1, i):
        if isPentagonal(P(i) - P(j)) and isPentagonal(P(i) + P(j)):
            print(P(i) - P(j))
            i = -1
            break
    i += 1

# NB: this does not give the proof that d is minimized

nTerms = [1]
res = 1
for i in range(2, 1000000):
    n = i
    nSteps = 0
    while n >= i:
        if n % 2:
            n = 3 * n + 1
        else:
            n //= 2
        nSteps += 1
    nTerms.append(nSteps + nTerms[n - 1])
    if nTerms[res - 1] < nTerms[-1]:
        res = i
print(res)

from util.properties import isPalindrome

res = 0
for i in range(999, 1, -1):
    for j in range(i, 1, -1):
        if isPalindrome(i * j):
            res = max(res, i * j)
print(res)

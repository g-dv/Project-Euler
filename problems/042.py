from util.properties import isTriangle

file = open('data/42.txt', 'r')
words = list(eval(file.read()))
file.close()

res = 0
for word in words:
    score = 0
    for letter in word:
        score += ord(letter) - 64
    if isTriangle(score):
        res += 1
print(res)

file = open('data/59.txt', 'r')
encrypted = [int(e) for e in file.readline().split(',')]
file.close()

def decrypt(encrypted, key):
    decrypted = []
    for i, e in enumerate(encrypted):
        decrypted.append(key[i % len(key)] ^ e)
    return decrypted

lowercaseRange = range(ord('a'), 1 + ord('z'))
for i in lowercaseRange:
    # cut time: check if we're very far from an english text
    if ''.join([chr(e) for e in decrypt(encrypted, [i, ord('a'), ord('a')])]).count(' ') < 10:
        continue
    for j in lowercaseRange:
        for k in lowercaseRange:
            decrypted = decrypt(encrypted, [i, j, k])
            string = ''.join([chr(e) for e in decrypted])
            if string.count(' ') > 200:
                print(sum(decrypted))

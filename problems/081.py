file = open('data/81.txt', 'r')
data = file.readlines()
file.close()

res = [] # cumulative matrix

for line in data:
    res.append([int(e) for e in line.split(',')])

for i in range(1, 80):
    res[i][0] += res[i - 1][0]
    res[0][i] += res[0][i - 1]
for i in range(1, 80):
    for j in range(1, 80):
        res[i][j] += min(res[i - 1][j], res[i][j - 1])
print(res[79][79])

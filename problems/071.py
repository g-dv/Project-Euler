from math import gcd

fractions = []
for d in range(999900, 1000001):
    n = int(3 * d / 7)
    fractions.append([n / d, n, d])
for res in sorted(fractions)[::-1]:
    if gcd(res[1], res[2]) == 1:
        print(res[1])
        break

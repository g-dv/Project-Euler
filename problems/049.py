from os import sys
from itertools import permutations, combinations
from util.factors import isPrime
from util.manips import concat

found = False
for i in range(1000, 10000):

    terms = list(set(permutations(str(i))))
    primePermutations = []
    for term in terms:
        number = int(''.join(term))
        if isPrime(number):
            primePermutations.append(number)

    if not primePermutations:
        continue
    if min(primePermutations) < 1000:  # only 4 digits
        continue
    if 1487 in primePermutations:  # case given as example
        continue

    # check if we can make the suite out of these primes
    triplets = combinations(primePermutations, 3)
    for triplet in triplets:
        triplet = sorted(triplet)
        if triplet[1:] == [triplet[0] + 3330, triplet[0] + 6660]:
            print(concat(triplet))
            found = True
            break
    if found:
        break

from itertools import combinations_with_replacement
from math import factorial
from util.manips import concat


# cf. https://en.wikipedia.org/wiki/Multinomial_theorem
def countArrangements(digits):
    occurences = [0] * 10
    for e in digits:
        occurences[e] += 1
    numerator = 0
    denominator = 1
    for e in occurences:
        numerator += e
        denominator *= factorial(e)
    numerator = factorial(numerator)
    return numerator // denominator


def cycleEndsWith89(n):
    while n != 89 and n != 1:
        tmp = n
        n = 0
        while tmp:
            n += (tmp % 10)**2
            tmp //= 10
    return n == 89


# the numbers { 123, 132, 213, ..., 1023, 1032, ..., 10023, ..., 3210000 } will
# have the same cycle!
combinations = list(combinations_with_replacement([0, 1, 2, 3, 4, 5, 6, 7, 8, 9], 7))
res = 0
for combination in combinations[1:]: # skip 0000000
    if cycleEndsWith89(concat(combination)):
        res += countArrangements(combination)
print(res)

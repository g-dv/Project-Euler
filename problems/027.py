from util.factors import isPrime

maxN = -1
for a in range(-999, 1000):
    for b in range(-999, 1000):
        n = 0
        while isPrime(n * n + a * n + b):
            n += 1
        if n > maxN:
            maxN = n
            res = a * b
print(res)

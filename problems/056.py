from util.manips import sumDigits

res = 0
for a in range(0, 100):
    for b in range(0, 100):
        res = max(res, sumDigits(a**b))
print(res)

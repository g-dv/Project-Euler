from util.manips import nDigits

res = 0
for i in range(1, 10):
    n = 1
    power = i**n
    while n <= nDigits(power):
        if nDigits(power) == n:
            res += 1
        n += 1
        power = i**n
print(res)

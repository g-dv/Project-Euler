from util.properties import isPandigital
from util.manips import firstDigits
from math import log


def fibonacciSequence():
    terms = [1, 1]
    while True:
        terms.append(terms[-1] + terms[-2])
        yield terms.pop(0)


for n, u in enumerate(fibonacciSequence()):
    if isPandigital(u % 10**9) and isPandigital(firstDigits(u, 9)):
        break
print(n + 1)

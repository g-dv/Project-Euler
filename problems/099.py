from math import log

file = open('data/99.txt', 'r')
data = file.readlines()
file.close()

numbers = [[int(x) for x in line.split(",")] for line in data]

res = -1
maxValue = -1
for i, e in enumerate(numbers):
    value = e[1] * log(e[0])
    if value > maxValue:
        maxValue = value
        res = 1 + i
print(res)

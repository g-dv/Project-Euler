from fractions import gcd

res = 0
for denominator in range(2, 12001):
    for numerator in range(1 + denominator // 3, (denominator + 1) // 2):
        if gcd(numerator, denominator) == 1:
            res += 1
print(res)

# H = outer height
# L = inner height
# h = outer length
# l = inner length

bestDifference = 200001
# We use 2000 because with a height of 1 and a length of 2000
# the grid can contain (2000 * 2001) / 2 = 2001000 rectangles.
for H in range(1, 2000):
    for L in range(1, 2000):
        number = (H * (H + 1)) / 2 * (L * (L + 1)) / 2
        difference = abs(number - 2000000)
        if difference < bestDifference:
            res = H * L
            bestDifference = difference
        if number > 2000000:
            break
print(res)

# line 11 is a simplification of:
#     number = 0
#     for h in range(0, H):
#         for l in range(0, L):
#             number += (H - h) * (L - l)
# since X + (X - 1) + ... + 1 = X * (X + 1) / 2

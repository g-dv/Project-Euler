from util.manips import nDigits
from util.factors import isPrime


def isCircularPrime(x):
    nRotations = nDigits(x)
    for i in range(nRotations):
        if not isPrime(x):
            return False
        x += 10**nRotations * (x % 10)
        x //= 10
    return True


res = 1
i = 3
while i < 10**6:
    if isCircularPrime(i):
        res += 1
    i += 2
print(res)

memory = [0] * 101
memory[0] = 1
for i in range(1, 100):
    for j in range(i, 101):
        memory[j] += memory[j - i]
print(memory[100])

# The example is wrong.
# Since 0 is a positive integer, 5 can be written in 7 ways (including 7 + 0).

from util.factors import primesBelow, isPrime
from util.manips import sumDigits, concat

# /!\ BEWARE: Ugly code. /!\

# A number is divisible by 3 iff the sum of its digits is.
# if sumDigits(n) % 3 == 1 and sumDigits(m) % 3 == 2, then
# sumDigits(concat(n, m)) % 3 = (1 + 2) % 3 = 0
# so we can treat the primes separately and make two groups:
# digitalSum % 3 == 1 and digitalSum % 3 == 2
primes = [[], []]
for prime in primesBelow(8500):  # guessed the upper limit
    if sumDigits(prime) % 3 == 1:
        primes[0].append(prime)
    else:  # sum % 3 == 2
        primes[1].append(prime)

pairs = [[], []]  # remember the primes that can be paired
for g in range(0, 2):
    for i in range(len(primes[g])):
        pairs[g].append([False] * len(primes[g]))
        for j in range(i + 1, len(primes[g])):
            a = primes[g][i]
            b = primes[g][j]
            pairs[g][-1][j] = isPrime(concat([a, b])) and isPrime(
                concat([b, a]))

res = 10**9

for g in range(0, 2):
    for i in range(len(primes[g])):
        if primes[g][i] * 5 >= res:
            break
        partialSum = primes[g][i]

        for j in range(i + 1, len(primes[g])):
            if partialSum + primes[g][j] * 4 >= res:  # we won't find better than what we already found
                break
            partialSum += primes[g][j]
            if not pairs[g][i][j]:  # check if the concatenations are primes
                continue

            for k in range(j + 1, len(primes[g])):
                if partialSum + primes[g][k] * 3 >= res:
                    break
                partialSum += primes[g][k]
                if not pairs[g][i][k] or not pairs[g][j][k]:
                    continue

                for m in range(k + 1, len(primes[g])):
                    if partialSum + primes[g][m] * 2 >= res:
                        break
                    partialSum += primes[g][m]
                    if not pairs[g][i][m] or not pairs[g][j][m] or not pairs[g][k][m]:
                        continue

                    for n in range(m + 1, len(primes[g])):
                        if partialSum + primes[g][n] >= res:
                            break
                        if not pairs[g][i][n] or not pairs[g][j][n] or not pairs[g][k][n] or not pairs[g][m][n]:
                            continue

                        res = min(res, primes[g][i] + primes[g][j] +
                                  primes[g][k] + primes[g][m] + primes[g][n])
print(res)

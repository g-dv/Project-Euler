LIMIT = 9999


def isPandigital(multiplicand, multiplier):
    product = multiplicand * multiplier
    string = str(multiplicand) + str(multiplier) + str(product)
    return ''.join(sorted(string)) == "123456789"


products = set()
for i in range(1, 1 + LIMIT):
    for j in range(i + 1, int(LIMIT / i)):
        if isPandigital(i, j):
            products.add(i * j)
print(sum(products))

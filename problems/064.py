class Step:
    def __init__(self, a, b, c, no):
        self.a = a
        self.b = b
        self.c = c
        self.no = no

    def equals(self, step):
        return self.a == step.a and self.b == step.b and self.c == step.c


res = 0
for n in range(10001):
    a0 = int(n**.5)
    if n**.5 != a0:
        memory = []
        step = Step(a0, 1, 0, 0)
        while True:
            memory.append(step)

            c = step.a * step.b - step.c
            b = (n - c * c) / step.b
            a = int((a0 + c) / b)
            step = Step(a, b, c, step.no + 1)

            identicalSteps = [e for e in memory if step.equals(e)]
            if len(identicalSteps):
                period = step.no - identicalSteps[0].no
                break
        if period % 2:
            res += 1
print(res)

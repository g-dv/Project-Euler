from util.properties import isPalindrome

res = 0
for n in range(1000000):
    binary = int(bin(n)[2:])
    if isPalindrome(n) and isPalindrome(binary):
        res += n
print(res)

def getCycleLength(denominator):
    # euclidian division until remainder is 1 once again
    for i in range(1, denominator):
        if 10**i % denominator == 1:
            return i
    return 0


longestCycle = -1
for i in range(2, 1000):
    length = getCycleLength(i)
    if length > longestCycle:
        res = i
        longestCycle = length
print(res)

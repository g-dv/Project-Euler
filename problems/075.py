from util.manips import pythagoreanTriples

count = dict()
for triplet in pythagoreanTriples(750000):
    length = sum(triplet)
    if length <= 1500000:
        if length in count:
            count[length] += 1
        else:
            count[length] = 1

res = 0
for length in count:
    if count[length] == 1:
        res += 1
print(res)

from util.manips import nDigits
from util.factors import isPrime


def isTruncable(n):
    if not isPrime(n):
        return False
    for i in range(1, nDigits(n)):
        leftShift = n % 10**i
        rightShift = n // 10**i
        if not isPrime(leftShift) or not isPrime(rightShift):
            return False
    return True


nFound = 0
res = 0
n = 10
while nFound != 11:
    if isTruncable(n):
        nFound += 1
        res += n
    n += 1
print(res)

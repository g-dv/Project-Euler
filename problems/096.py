from copy import deepcopy
from util.manips import concat


MULTIPLE = 10


def readGrids():
    file = open('data/96.txt', 'r')
    data = file.read().splitlines()
    file.close()

    grids = []
    for i in range(len(data) // 10):
        grid = []
        for j in range(1, 10):
            grid.append([int(e) for e in data[10 * i + j]])
        grids.append(grid)
    return grids


def canPut(digit, i, j):
    for k in range(9):
        if digit == grid[k][j] or digit == grid[i][k]:
            return False
    for m in range(3):
        for n in range(3):
            if digit == grid[m + i - i % 3][n + j - j % 3]:
                return False
    return True


def isFilled():
    for i in range(9):
        for j in range(9):
            if not grid[i][j]:
                return False
    return True


def fillGaps():
    # returns True  if we successfully filled some gaps
    #         False if no gaps were filled
    #         None  if the grid is invalid
    if isFilled():
        return True
    gapsFilled = False
    for i in range(9):
        for j in range(9):
            if not grid[i][j]:
                fittingDigit = None
                for k in range(1, 10):
                    if canPut(k, i, j):
                        if fittingDigit == None:
                            fittingDigit = k
                        else:
                            fittingDigit = MULTIPLE
                            break
                if fittingDigit == None:
                    return None
                elif fittingDigit != MULTIPLE:
                    grid[i][j] = fittingDigit
                    gapsFilled = True
    return fillGaps if gapsFilled else False


def findFirstZero():
    for i in range(9):
        for j in range(9):
            if not grid[i][j]:
                return i, j
    return None, None


def backtrack():
    global grid
    previousGrid = deepcopy(grid)
    status = fillGaps()
    if status == None:
        grid = previousGrid # abort
        return False
    elif status == True or isFilled():
        return True
    else:
        i, j = findFirstZero()
        for k in range(1, 10):
            if canPut(k, i, j):
                grid[i][j] = k
                if backtrack():
                    return True
        grid = previousGrid # abort
        return False


grids = readGrids()
res = 0
for grid in grids:
    backtrack()
    res += concat(grid[0][:3])
print(res)

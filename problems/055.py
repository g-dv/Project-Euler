from util.properties import isPalindrome
from util.manips import reverse

res = 0
for i in range(1, 10000):
    n = i
    res += 1
    for j in range(0, 50):
        n = n + reverse(n)
        if isPalindrome(n):
            res -= 1
            break
print(res)

from util.properties import isPermutation
from util.factors import primesBelow

minRatio = 2
for i, p in enumerate(primesBelow(4000)):
    for q in primesBelow(4000)[i + 1:]:
        n = p * q
        if n > 10000000:
            break
        phi = n - p - q + 1
        # eg. n = 3 * 7 = 21 => phi = 20 - len({ x | x < 21, 3|x or 7|x }
        #                    => phi = 20 - 6 - 2 = 21 + 1 - 7 - 3
        if isPermutation(n, phi) and n / phi < minRatio:
            minRatio = n / phi
            res = n
print(res)

# my program gives the solution but doesn't prove that this is the solution
# it doesn't seem plausible but the solution could in fact be obtained from
# more than two primes

# I guessed that there weren't any repeating digits
# Then, by hand:

# the digits to order are : 0, 1, 2, 3, 6, 7, 8, 9
# 319 & 129 => 3129
# 3129 & 736 => 73129
# 73129 & 890 => 731290
# 731290 & 162 => 7316290
# 7316290 & 289 => 73162890

print(73162890)

from itertools import permutations
from util.factors import isPrime
from os import sys

found = False
for i in range(9, 0, -1):
    pandigitals = list(permutations('123456789' [:i]))
    for j in range(1, len(pandigitals) + 1):
        number = int(''.join(pandigitals[-j]))
        if isPrime(number):
            found = True
            break
    if found:
        break
print(number)

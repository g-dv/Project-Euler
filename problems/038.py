from util.properties import isPandigital
from util.manips import concat

for n in range(192, 10000):  # can't be more than a 4 digits number
    i = 2
    concatenation = n
    while concatenation < 987654322:
        if isPandigital(concatenation):
            res = concatenation
            break
        concatenation = concat([concatenation, i * n])
        i += 1
print(res)

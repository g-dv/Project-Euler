FIRST_PRIMES = [2, 3, 5, 7, 11, 13, 17, 19, 23]

res = 1
for p in FIRST_PRIMES:
    if res * p > 10**6:
        break
    res *= p
print(res)

# should be a multiple of as many small primes as possible
# smaller primes will lead to a smaller phi
# and we must take the biggest n, so the answer is
# 2 * 3 * 5 * 7 * ... while the product is under 1 000 000

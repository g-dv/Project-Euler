from itertools import permutations
from util.manips import concat

def isValid(arrangement):
    return (arrangement[0] + arrangement[1] + arrangement[2]
        == arrangement[3] + arrangement[2] + arrangement[4]
        == arrangement[5] + arrangement[4] + arrangement[6]
        == arrangement[7] + arrangement[6] + arrangement[8]
        == arrangement[9] + arrangement[8] + arrangement[1])

def toString(arrangement):
    return concat([
        arrangement[0], arrangement[1], arrangement[2],
        arrangement[3], arrangement[2], arrangement[4],
        arrangement[5], arrangement[4], arrangement[6],
        arrangement[7], arrangement[6], arrangement[8],
        arrangement[9], arrangement[8], arrangement[1]
    ])

# We already know it will can by 6, 5, 3 since the inner values
# must be 1-5 and the outer 6-10 and 6 is the smallest outer value.
# Length can't be 17 since 10 will in an outer value.
arrangements = list(permutations([1, 2, 4, 7, 8, 9, 10]))

for arrangement in arrangements:
    completeArrangement = [6, 5, 3] + list(arrangement)
    if isValid(completeArrangement):
        print(toString(completeArrangement))
        break

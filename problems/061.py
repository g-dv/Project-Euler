def triangles(minIndex, maxIndex):
    return [(i * i + i) // 2 for i in range(minIndex, maxIndex + 1)]


def squares(minIndex, maxIndex):
    return [i * i for i in range(minIndex, maxIndex + 1)]


def pentagonals(minIndex, maxIndex):
    return [(3 * i * i - i) // 2 for i in range(minIndex, maxIndex + 1)]


def hexagonals(minIndex, maxIndex):
    return [2 * i * i - i for i in range(minIndex, maxIndex + 1)]


def heptagonals(minIndex, maxIndex):
    return [(5 * i * i - 3 * i) // 2 for i in range(minIndex, maxIndex + 1)]


def octogonals(minIndex, maxIndex):
    return [3 * i * i - 2 * i for i in range(minIndex, maxIndex + 1)]


# we choose the index to select only numbers between 1000 and 9999
numbers = [
    triangles(45, 140),
    squares(32, 99),
    pentagonals(26, 81),
    hexagonals(23, 70),
    heptagonals(21, 63),
    octogonals(19, 58)
]


class Node:
    def __init__(self, group, number):
        self.neighbors = []
        self.number = number
        self.group = group


def backtrackPath(mask, path, target):
    mask[path[-1].group] = True  # remember the groups we already visited
    if mask == [True] * 6:  # we visited all the groups
        # now check if we can reach the initial node
        for neighbor in path[-1].neighbors:
            if neighbor == target:  # we can close the loop
                print(sum([e.number for e in path]))
                return
        return
    for neighbor in path[-1].neighbors:
        if not mask[neighbor.group]:  # we didn't visit this group
            backtrackPath(mask.copy(), path + [neighbor], target)


# create the nodes
groups = []
for i in range(0, 6):
    groups.append([])
    for e in numbers[i]:
        groups[i].append(Node(i, e))

# create the directional arcs
for i, srcGroup in enumerate(groups):
    for j, srcNumber in enumerate(numbers[i]):
        for k, destGroup in enumerate(groups):
            if srcGroup != destGroup:
                for m, destNumber in enumerate(numbers[k]):
                    # check if the end matches the beginning
                    if srcNumber % 100 == destNumber // 100:
                        srcGroup[j].neighbors.append(destGroup[m])

# test all the paths
for node in groups[0]:  # we can start from any group
    backtrackPath([False] * 6, [node], node)  # target is initial node

file = open('data/82.txt', 'r')
data = file.readlines()
file.close()

res = [] # cumulative matrix

for line in data:
    res.append([int(e) for e in line.split(',')])

for i in range(1, 80):
    # this time, we can start anywhere in the 1st column 
    res[0][i] += res[0][i - 1]
for j in range(1, 80): # we proceed column by column
    original = [row[j] for row in res] # save previous value
    for i in range(1, 80): # decide between coming from up or left
        res[i][j] += min(res[i - 1][j], res[i][j - 1])
    for i in range(78, -1, -1): # go up
        res[i][j] = min(res[i][j], original[i] + res[i + 1][j])
    # Since we cannot reach the last row by going up, the last value of the
    # column is right after the first loop. So, using this good value, we go up
    # to check if it would have been faster coming from down.
print(min([row[79] for row in res]))

from util.properties import isTriangle, isPentagonal, isHexagonal

n = 144
while True:
    Hn = n * (2 * n - 1)
    if isTriangle(Hn) and isPentagonal(Hn):
        print(Hn)
        break
    n += 1

from random import randint

# We don't need a huge precision so I guess simulation will be ok.
# Edit: I disliked this problem. It's just a stupid simulation.
#       They should have asked for a better precision.


def roll():
    global position

    rolls = [randint(1, 4), randint(1, 4)]

    if rolls[0] == rolls[1]:
        roll.counter += 1
    else:
        roll.counter = 0
    if roll.counter == 3: # 3 doubles => jail
        position = 10
        count[position] += 1
        roll.counter = 0
        return roll() # now we roll again for next turn

    return sum(rolls)


roll.counter = 0


def drawCH(number, position):
    threeBehind = (position - 3) % 40
    if position == 7:
        nextRail = 15
        nextUtility = 12
    elif position == 22:
        nextRail = 25
        nextUtility = 28
    else:
        nextRail = 5
        nextUtility = 12
    ISSUES = [0, 10, 11, 24, 39, 5, nextRail, nextRail, nextUtility, threeBehind]
    return ISSUES[number - 1] if number < 11 else position


def drawCC(number, position):
    ISSUES = [0, 10]
    return ISSUES[number - 1] if number < 3 else position


count = [0] * 40
position = 10
for _ in range(100000):
    position = (position + roll()) % 40

    if position in [2, 17, 33]: # CC
        position = drawCC(randint(1, 16), position)
    elif position in [7, 22, 36]: # CH
        position = drawCH(randint(1, 16), position)
    elif position == 30: # G2J
        position = 10

    count[position] += 1

res = ''
for i in range(3):
    mostVisited = count.index(max(count))
    res += str(mostVisited + i).zfill(2)
    count.pop(mostVisited)
print(res)

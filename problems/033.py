from fractions import gcd

productD = 1
productN = 1
for n in range(10, 100):
    for d in range(n + 1, 100):
        if d % 10:  # exclude trivial cases
            # n % 10 and d // 10 are the only digits that we need to compare
            # since only them could make matching fractions once removed
            if n % 10 == d // 10:
                # remaining digits
                tinyN = n // 10
                tinyD = d % 10
                if n / d == tinyN / tinyD:
                    productN *= tinyN
                    productD *= tinyD
print(productD // gcd(productN, productD))

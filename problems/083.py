def visitCell(i, j, score):
    global res, original, queue
    newScore = original[i][j] + score
    if res[i][j] > newScore:
        res[i][j] = newScore
        queue.append([i, j]) # mark this neighbor to visit him later


file = open('data/83.txt', 'r')
data = file.readlines()
file.close()

original = [[int(e) for e in line.split(',')] for line in data]
res = [[10**9 for i in range(80)] for j in range(80)]

res[0][0] = original[0][0]

# Classic BFS
queue = [[0, 0]]
while len(queue):
    [i, j] = queue.pop(0)
    if i > 0:
        visitCell(i - 1, j, res[i][j])
    if i < 79:
        visitCell(i + 1, j, res[i][j])
    if j > 0:
        visitCell(i, j - 1, res[i][j])
    if j < 79:
        visitCell(i, j + 1, res[i][j])
print(res[79][79])

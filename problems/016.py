power = pow(2, 1000)
sum = 0
while power:
    sum += power % 10
    power //= 10
print(sum)

from util.factors import primeFactors

i = 1
memory = [1, 1, 1]
while True:
    memory.append(len(set(primeFactors(i + 3))))
    isValid = True
    for j in range(4):
        if memory[j] != 4:
            isValid = False
            break
    if isValid:
        print(i)
        break
    memory.pop(0)
    i += 1

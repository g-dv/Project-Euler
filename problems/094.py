def checkParameter(parameter):
    return parameter == int(parameter) and parameter > 0


def checkSolution(perimeter, area):
    return checkParameter(area) and checkParameter(perimeter)


MAX_PERIMETER = 1000000000

x = 2
y = 1
perimeter = 3
res = 0

while perimeter <= MAX_PERIMETER:
    area = y * (x - 2) / 3
    if checkSolution((perimeter - 1) / 3, area):
        res += perimeter

    area = y * (x + 2) / 3
    if checkSolution((perimeter + 1) / 3, area):
        res += perimeter

    x, y = (2 * x) + (3 * y), (2 * y) + (x)

    perimeter = 2 * x

print(res)

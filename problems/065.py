from util.manips import sumDigits

# generate [2, 1, 2, 1, 1, 4, 1, 1, 6, 1, ..., 1, 2k, 1, ...]
terms = [2]
for i in range(1, 34):
    terms += [1, i * 2, 1]

numerator = terms[-1]
denominator = 1
for i in range(100 - 2, -1, -1):
    tmp = numerator
    numerator = terms[i] * numerator + denominator
    denominator = tmp
print(sumDigits(numerator))

# a + 1 / (n / d) = a + d / n
#                 = (an + d) / n
# So:
#     denominator(n+1) = numerator(n)
#     numerator(n+1) = term[n+1] * numerator(n) + denominator(n)

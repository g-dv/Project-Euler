from util.factors import primeFactors
from util.manips import product
from itertools import combinations

res = 0
for denominator in range(2, 1000000 + 1):
    factors = set(primeFactors(denominator))
    sign = -1
    res += denominator
    for i in range(len(factors)):
        for partition in combinations(factors, i + 1):
            res += sign * (denominator // product(partition))
        sign = -sign
print(res)

from util.factors import isPrime

nPrimes = 3
current = 9
halfLength = 2
while nPrimes / (4 * (halfLength - 1)) > 0.1:
    for i in range(1, 4):  # no need to check the square diagonal
        current += 2 * halfLength
        nPrimes += isPrime(current)
    current += 2 * halfLength
    halfLength += 1
print(2 * halfLength - 1)

from util.manips import nDigits


class Fraction:
    def __init__(self, n, d):
        self.n = n
        self.d = d


res = 0
sequence = [Fraction(1, 2)]
for n in range(0, 1000):
    last = sequence[-1]
    new = Fraction(last.d, 2 * last.d + last.n)
    sequence.append(new)
    res += nDigits(new.n + new.d) > nDigits(new.d)
print(res)

# v0 = 1/2
# v1 = 1 / (2 + v0)
# ..
# vn+1 = 1 / (2 + vn)
#      = 1 / (2 + vn.n / vn.d)
#      = 1 / ((2 * vn.d + vn.n) / vn.d)
#      = vn.d / (2 * vn.d + vn.n)

# un = 1 + vn
#    = 1 + vn => un.n = vn.d + vn.n
#    = 1 + vn => un.d = vn.d

# we compare the number of digits of the numerator and denominator of un

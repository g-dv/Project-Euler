#include <iostream>

using namespace std;

unsigned int gcd (unsigned int n1, unsigned int n2) {
    return (n2 == 0) ? n1 : gcd (n2, n1 % n2);
}

int main()
{
    int res = 0;
    for (int denominator = 2; denominator <= 12000; denominator++)
        for (int numerator = 1 + denominator / 3; numerator < (denominator + 1) / 2; numerator++)
            if (gcd(numerator, denominator) == 1)
                res += 1;
    cout << res << endl;
    return 0;
}

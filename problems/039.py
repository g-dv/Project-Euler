from math import hypot

MAX_P = 1000
MAX_A = 1 + MAX_P // 2

solutions = [0] * (MAX_P + 1)
for a in range(1, MAX_A):
    b = a + 1
    while True:
        p = a + b + hypot(a, b)
        if p > MAX_P:
            break
        elif p == int(p):
            solutions[int(p)] += 1
        b += 1
print(solutions.index(max(solutions)))
